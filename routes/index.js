const express = require("express");
const router = express.Router();
const MarkdownIt = require("markdown-it");
const admin = require("firebase-admin");
const fs = require("fs");
const readline = require("readline");
const { google } = require("googleapis");
const twitterAPI = require("node-twitter-api");
const moment = require("moment");

let twitterCreds = JSON.parse(fs.readFileSync("twittercred.json"));

var twitter = new twitterAPI(twitterCreds);

let tweets = [];

function updateTwitter() {
  twitter.getTimeline(
    "user",
    { screen_name: "slponycon", count: 3 },
    twitterCreds.accessToken,
    twitterCreds.accessTokenSecret,
    (err, data, response) => {
      if (err) {
        console.log(err);
      } else {
        tweets = [];
        tweets.push(data);
      }
    }
  );
}

const SCOPES = ["https://www.googleapis.com/auth/calendar.readonly"];
const TOKEN_PATH = "../credentials.json";

let content = {};
let slEvents = [];

function getCalendars() {
  fs.readFile("credentials.json", (err, content) => {
    if (err) return console.log("Error loading client secret file:", err);
    // Authorize a client with credentials, then call the Google Calendar API.
    authorize(JSON.parse(content), listEvents);
  });
}

getCalendars();
updateTwitter();

setInterval(() => getCalendars(), 30000);
setInterval(() => updateTwitter(), 30000);

function authorize(credentials, callback) {
  const { client_secret, client_id, redirect_uris } = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(
    client_id,
    client_secret,
    redirect_uris[0]
  );

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getAccessToken(oAuth2Client, callback);
    oAuth2Client.setCredentials(JSON.parse(token));
    callback(oAuth2Client);
  });
}

function getAccessToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: "offline",
    scope: SCOPES,
  });
  console.log("Authorize this app by visiting this url:", authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question("Enter the code from that page here: ", (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error("Error retrieving access token", err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) return console.error(err);
        console.log("Token stored to", TOKEN_PATH);
      });
      callback(oAuth2Client);
    });
  });
}

function listEvents(auth) {
  const calendar = google.calendar({ version: "v3", auth });
  calendar.events.list(
    {
      calendarId: "slponycon@gmail.com",
      timeMin: new Date().toISOString(),
      maxResults: 100,
      singleEvents: true,
      orderBy: "startTime",
    },
    (err, res) => {
      if (err) return console.log("The API returned an error: " + err);
      const events = res.data.items;
      if (events.length) {
        slEvents = [];
        events.map((event, i) => {
          const start =
            new Date(event.start.dateTime) || new Date(event.start.date);
          slEvents.push({ start, name: event.summary });
        });
      } else {
        console.log("No upcoming events found.");
      }
    }
  );
}

var serviceAccount = require("../serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://slponycon-db250.firebaseio.com",
});

admin
  .database()
  .ref("/")
  .on("value", function (snapshot) {
    content = snapshot.val();
    content.news.sort((a, b) => {
      return b.published - a.published;
    });
    //console.log(content);
  });

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", {
    title: "SLPonyCon 2020 | Brony Convention in Second Life!",
    content,
    moment,
    md: new MarkdownIt({ html: true, breaks: true, typographer: true }),
  });
});

router.get("/livestream", function (req, res, next) {
  res.render("twitch", {
    title: "Livestream and Schedule | SLPonyCon 2020",
    content,
    slEvents,
    md: new MarkdownIt({ html: true, breaks: true, typographer: true }),
  });
});

router.get("/vendors", function (req, res, next) {
  res.render("sellers", {
    title: "Vendors and Artists | SLPonyCon 2020",
    content,
    slEvents,
    md: new MarkdownIt({ html: true, breaks: true, typographer: true }),
  });
});

router.get("/api/events", function (req, res, next) {
  console.log();
  res.json(slEvents);
});

router.get("/api/tweets", function (req, res, next) {
  console.log();
  res.json(tweets);
});

router.get("/news/:slug", function (req, res, next) {
  const article = content.news.filter((el) => el.slug == req.params.slug)[0];
  console.log(article);
  res.render("article", {
    article,
    md: new MarkdownIt({ html: true, breaks: true }),
  });
});

module.exports = router;
